namespace Planavia.Controls;

public partial class Compass : ContentView
{
    public static readonly BindableProperty CourceProperty =
        BindableProperty.Create(nameof(Cource), typeof(int), typeof(Label), default(int), propertyChanged: null);

    public static readonly BindableProperty WindCourceProperty =
            BindableProperty.Create(nameof(WindCource), typeof(int), typeof(Label), default(int), propertyChanged: null);

    public int Cource
    {
        get => (int)GetValue(CourceProperty);
        set => SetValue(CourceProperty, value);
    }

    public int WindCource
    {
        get => (int)GetValue(WindCourceProperty);
        set => SetValue(WindCourceProperty, value);
    }

    public Compass()
    {
        InitializeComponent();
    }
}