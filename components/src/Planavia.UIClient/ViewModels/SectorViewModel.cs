﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace Planavia.ViewModels
{
    public sealed partial class SectorViewModel : ObservableObject
    {
        [ObservableProperty]
        private int _cource;
        [ObservableProperty]
        private double _courceCorrection;
        [ObservableProperty]
        private int _distance;
        [ObservableProperty]
        private int _speed;
        [ObservableProperty]
        private double _speedCorrection;
        [ObservableProperty]
        private TimeSpan _time;
        [ObservableProperty]
        private int _windCource;
        [ObservableProperty]
        private int _windSpeed;
        [RelayCommand]
        public void CaclulateCorrection()
        {
            try
            {
                double maxDeclinationAngle = 60 * WindSpeed / Speed;
                double speedAndle = WindCource - Cource;

                double declinationAngle = Math.Sin(speedAndle * (Math.PI / 180.0)) * maxDeclinationAngle;
                double frontalWindSpeed = Math.Cos(speedAndle * (Math.PI / 180.0)) * WindSpeed;

                SpeedCorrection = -Math.Round(frontalWindSpeed, 0);
                CourceCorrection = Math.Round(declinationAngle, 0);

                if (Speed <= 0)
                {
                    throw new InvalidOperationException("Скорость должна быть больше нуля.");
                }

                // Вычисляем время в часах
                double timeInHours = Distance / (Speed + SpeedCorrection);

                // Преобразуем время в часах в TimeSpan
                Time = TimeSpan.FromHours(Math.Round(timeInHours, 2));
            }
            catch
            {

            }
        }
    }
}
