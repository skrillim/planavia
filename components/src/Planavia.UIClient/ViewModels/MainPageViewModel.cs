﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System.Collections.ObjectModel;

namespace Planavia.ViewModels
{
    public sealed partial class MainPageViewModel : ObservableObject
    {

        public ObservableCollection<SectorViewModel> Sectors { get; set; } = new ObservableCollection<SectorViewModel>();

        [RelayCommand]
        private void AddSector()
        {
            Sectors.Add(new SectorViewModel());
        }
    }
}