﻿namespace Planavia.Models
{
    public sealed class Sector
    {
        public int Cource { get; set; }
        public int WindCourse { get; set; }
        public int WindSpeed { get; set; }
    }
}
